from time import sleep 
import random
random.seed()
import os


def clear_terminal():
    os.system('cls' if os.name == 'nt' else 'clear')
def sleep_pause(text,sleep_timer):
      text = print(text)
      sleep(sleep_timer)
def play_again():
        while True:
                command = input("Do You Want To Play Again ?(y/n)")
                if command =="y":
                        
                        sleep_pause("You Are starting a new game...please wait",2)
                        clear_terminal()
                        sleep(0.7)
                        adventure_game()
                        
                elif command =="n":
                        sleep(0.7)
                        print("As You want you can play again any time you want :)")
                        exit()
                else:
                        sleep(0.7)
                        print("Invalid Input Choose From (y/n)")
def choose_direction(what_is_happening,if_left,if_right):
            while True:
                direction = input(what_is_happening + " which direction will you choose?(right/left) : ")
                    
                if direction == 'left':
                        print(if_left)
                        break
                elif direction == 'right':   
                        print(if_right)
                        break
                else:
                        print("Invalid Input Choose From (left/right)")
            return direction
def choose_direction2(what_is_happening,if_direction_1,if_direction_2,if_direction_3):
            while True:
                direction_2=input(what_is_happening+ "where will you go(Climb a tree/go to the river ,there is no trees there may be there is a signal/climb the mountain) : ")
                if direction_2== "climb a tree":
                    sleep_pause(if_direction_1,2)
                    break
                elif direction_2=="go to the river":
                    sleep_pause(if_direction_2,2)
                    break
                elif direction_2=="climb the mountain":
                    sleep_pause(if_direction_3,2)
                    break
                else:
                    sleep_pause("Invalid Input Choose From (climb a tree/go to the river/climb the mountain)",2)
            return direction_2
def adventure_game():

    sleep_pause("Welcome To My Adventure Game",1)  
    
    while True:
        playing_confirmation=input("Are You Ready For The adventure? ?(y/n) :")
        if playing_confirmation == "y":
            break
        elif playing_confirmation == "n":
            print("You said you aren't ready,when you are ready try again..")
            exit()
        else:
            sleep_pause("Invail Input! choose from (y/n)!",2)
    
    sleep_pause("let's Start",1)
    name = input("Enter Your Name : ")
    sleep(0.5)
    sleep_pause("Welcome "+name+" To The Game",1)
    sleep_pause("You Are Now In A Jungle Forest You Want To Survive And Go out From Here",1)
    health = '100'
    sleep(0.5)
    total_score = '0'
    sleep_pause("Your Health = "+ health,0.5)
    sleep_pause("Your score = "+ total_score,0.5)
    sleep_pause("Try To get maximum total_score you can , also If your health Run out you will lose",2) 
    
    direction =choose_direction("You Are at end of a road you have only 2 direction",
                     "Great You Have Choosen left let's see what you will find !",
                     "Great You Have Choosen Right let's see what you will find !")
          
    sleep(1)
    sleep_pause("It's So Dark Her You are walking with out seeing anything",2)
    print("....")
    sleep(2)
    while True:           
        if direction == "right":
            health = 100
            health -= 100
            fatal_events =["A tree Had Fallen On You !" , "You Felt In a fatal hole","You Felt in A River !"]
            random_fatal_events = random.choice(fatal_events)

            print(random_fatal_events)
            if random_fatal_events == "You Felt in A River !":
                sleep_pause("Oh dear you don't know how to swim !",1.5)
                
                print("You Have Drowned ")
            elif random_fatal_events == "You Felt In a fatal hole" :
                print("it is too deep !")
            else:
                sleep_pause("The tree is enormus ",1)
            
            print("Your Health now = ", health)
            sleep(1)

            break
        elif direction == "left":
            total_score = 0
            health = 100
            total_score += 50
            print("good Job you find the right way! Your total_score now is" , total_score)
            sleep(1.2)
            sleep_pause("wait a second what is this you cannot see well here!",1)
            wild_animals = ["Tiger", "Snake", "Lion","cheetah" ]
            random_wild_animal = random.choice(wild_animals)

            sleep_pause("You Found a "+random_wild_animal+" !",1)
            sleep_pause("You are fighting The "+random_wild_animal+" now",1)
            sleep_pause("you defeat the "+random_wild_animal,1.5)
            total_score += 30
            health -= 30
            
            print("Good Job your score is now ", total_score )
            sleep(0.9)
            print("and your health is now ", health)
            sleep(2)
            break
    if health == 0:
            print("Game Over !")
            print("Try Another Option next time :( ")
            play_again()
    sleep_pause("Good Job You passed the first stage! 100 Points were added to your score",1)       
    total_score += 100          
    sleep_pause("You have Found A box",0.5)
    sleep_pause("let's open it and see what will we find",1)
    sleep_pause("Congrats! You Have Found A Cell Phone",1)
    sleep_pause("Let's Call help !",1)
    while True:
          call_confrimation=input("Who you want to call ?(mum/police)")
          if call_confrimation=="mum":
                sleep_pause("You Can't remember Your mum's number",1)
                print("Try calling police")
          elif call_confrimation=="police":
                sleep_pause("You are dialing 911 now !",0.5)
                break

          else:
                sleep_pause("Invail Input! choose from (mum/police)!",1)
    sleep_pause("...",0.5)
    sleep_pause("It seems that there is no signal here!Try going a higher place ",2)
    direction2 = choose_direction2("Now You should move to a higher place ","You Have Choosen to Climb a tree","You Have choosen to go to the river","You have choosen to climb the mountain")
    random.seed()
    while True:
      tree_events=["You have climbed a bough that was too weak","Oh watch out there is a monekys !"]
      mountain_events=["Oh watch out those Hawks !","Oh watch out this avalanche !"]
      random_tree_events = random.choice(tree_events)
      random_mountain_events = random.choice(mountain_events)
      if direction2=="climb a tree":
            sleep_pause(random_tree_events,2)
            if random_tree_events == "You have climbed a bough that was too weak":
                  sleep_pause("the bough broke !",1)
            elif random_tree_events=="Oh watch out there is a monekys !":
                  sleep_pause("The monekeys are attacking you !",1)
            sleep_pause("You have fallen on the Ground !",1)
            sleep_pause("Your health is 0 now !",1)
            health -= 70
            if health == 0:
                sleep_pause("Game Over !",1)
                print("Try Another Option next time :( ")
                play_again()
      elif direction2 == "climb the mountain":
            sleep_pause(random_mountain_events,2)
            if random_mountain_events == ("Oh watch out those Hawks !"):
                  sleep_pause("The Hawks are atacking You !",1)

            elif random_mountain_events == ("Oh watch out this avalanche !"):
                  sleep_pause("You Have burried in the snow !",1)
            sleep_pause("Your health is 0 now !",1)
            health -= 70
            if health == 0:
                sleep_pause("Game Over !",1)
                print("Try Another Option next time :( ")
                play_again()
      elif direction2 == "go to the river":
            sleep_pause("Congrats ! You found a signal !",1)
            total_score += 30
            sleep_pause("Your score increased 30 !",1)
            print("Your score Now is :" ,total_score)
            sleep(2)
            break
    sleep_pause("You are about to call the police",1)
    sleep_pause("Dialing...911",2)
    sleep_pause("Dispatcher : This is 911 what is your emergency ?",2)

    dispatcher_response=["Do You mean that you have stuck in the forest","are you saying that you have stuck in the forest's river?",
                            "is it a prank call ?","Are You Joking With Police !","are you saying that you lost in the sea?"]
    dispatcher_resonse2=["Sir, Calm down and speak slowely ","Sir, I can't hear you speak slower"]
    while True:
        dispatche=random.choice(dispatcher_response)
        dispatche2=random.choice(dispatcher_resonse2)
        emergency_situation = input("Describe Your Emergency Situation :")
        sleep_pause("You :"+ emergency_situation ,2)
        sleep_pause("Dispatcher : "+ dispatche2 , 2)
        while True:
            emergency_confirmation = input(dispatche+" (y/n)")
            if emergency_confirmation == "y":
                break
            elif emergency_confirmation == "n":
                sleep_pause("Dispatcher : Please Tell me Your Emergency situation again",1)
                break
            else:
                sleep_pause("Invalid input choose from (y/n)",1)
        if emergency_confirmation == "y":
            break
    while True:
        location=input("Dispatcher : Roger That Sir, Give me your location and the Help on the way :")
        sleep_pause("You :"+location,1.5)
        while True:
            location_confirmation=input("Dispatcher : Your Location is '" + location+"' do you confirm it ,sir ?(y/n)")
            if location_confirmation == "y" :
                break
            elif location_confirmation =="n":
                 sleep_pause("Please Give me your location again",1.5)
                 break
            else:
                 sleep_pause("Invalid input choose from (y/n)",1.5)
        if location_confirmation == "y":
             break
    sleep_pause("Dispatcher : Don't move from your location sir , a rescue chopper coming to rescue you",1.5)
    sleep_pause("Dispatcher : ETA : 10 min",1.5)
    sleep_pause("Dispatcher : Your Call number is 107-551",1.5)
    sleep_pause("...",2)
    sleep_pause("You can see The rescue chopper now !",1.5)
    sleep_pause("Rescue chopper has found you !",2)
    sleep_pause("Rescures are rappelling down from the chopper",1.5)
    total_score += 150
    sleep_pause("They Got You! You are safe now ",1.5)
    sleep_pause("Congratulations! You just won the game !",2)
    print("Your total_score is : ",total_score)
    sleep(3)
    sleep_pause("Thanks For playing <3")
    play_again()


adventure_game()
